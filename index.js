const Matrix = require('./Matrix');

const matrix = new Matrix();
const spiral = matrix.getSpiralSequence();
const sequence = spiral.map(({ coord, value }) => [ `${coord.y}${coord.x}`, value ]);

matrix.log();
console.table(sequence);
