const CursorSpiral = require('./Cursor/Spiral');
const { random }   = require('./helpers')

/**
 * Класс для создания квадратной матрицы с функционалом для управления данными
 *
 * @type {Matrix}
 */
module.exports = class Matrix {

    /**
     * Инициализация полей (по умолчанию размер случайный)
     *
     * @constructor
     * @params {number} [size] - размер матрицы
     * @this Matrix
     */
    constructor(size = 0) {
        this.size = size || Matrix.generateSize();
        this.data = this.getRandomizedData();
    }

    /**
     * Возвращает спиральную последовательность
     *
     * @param {boolean} [clockwise] - направление спирали (по часовой стрелке или нет)
     * @this Matrix
     * @return {Array}
     */
    getSpiralSequence(clockwise = true) {
        const coord  = this.getCenter();
        const cursor = new CursorSpiral({ coord, clockwise });
        const result = [];
        const counter = this.size ** 2 - 1;

        result.push(cursor.getFrom(this.data));

        for (let i = counter; i--;) {
            cursor.moveNext();
            result.push(cursor.getFrom(this.data));
        }

        return result;
    }

    /**
     * Возвращает двумерный массив со случайным числом в интервале, который указан
     * в статических геттерах
     *
     * @this Matrix
     * @return {Array[][]}
     */
    getRandomizedData() {
        return new Array(this.size)
            .fill()
            .map(() => new Array(this.size)
                .fill()
                .map(() => random(this.constructor.minValue, this.constructor.maxValue)));
    }

    /**
     * Возвращает координаты центра матрицы
     *
     * @this Matrix
     * @return {{x: number, y: number}}
     */
    getCenter() {
        const coord = Math.round(this.size / 2) - 1;

        return {
            x: coord,
            y: coord
        };
    }

    /**
     * Вывод матрицы в виде таблички
     *
     * @this Matrix
     */
    log() {
        console.table(this.data);
    }

    /**
     * Генерация размера матрицы для случая, если он не указан явно
     *
     * @static
     * @this Matrix
     * @return {number}
     */
    static generateSize() {
        return 2 * random(this.minSize, this.maxSize) - 1;
    }

    /**
     * Минимальное значение элемента матрицы
     *
     * @static
     * @getter
     * @this Matrix
     * @return {number}
     */
    static get minValue() {
        return 0;
    }

    /**
     * Максимальное значение элемента матрицы
     *
     * @static
     * @getter
     * @this Matrix
     * @return {number}
     */
    static get maxValue() {
        return 100;
    }

    /**
     * Минимальный размер матрицы для автогенерации
     *
     * @static
     * @getter
     * @this Matrix
     * @return {number}
     */
    static get minSize() {
        return 2;
    }

    /**
     * Максимальный размер матрицы для автогенерации
     *
     * @static
     * @getter
     * @this Matrix
     * @return {number}
     */
    static get maxSize() {
        return 5;
    }

}
