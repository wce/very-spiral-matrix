module.exports = class CursorSpiral {

    constructor(options = {}) {
        const { direction, coord, clockwise } = options;

        this.coord     = Object.assign(this.constructor.defaultCoord, coord || {})
        this.direction = this.constructor.availableDirections.includes(direction) ? direction : this.constructor.defaultDirection;
        this.clockwise = clockwise === true || clockwise === false ? clockwise : this.constructor.defaultClockwise;

        this.steps = 1;
        this.step  = this.steps;
        this.turn  = this.constructor.turns;
    }

    moveNext() {
        this[ this.getDirectionMethod() ]();
        this.updateStep();
    }

    moveUp() {
        this.coord.y--;
    }

    moveDown() {
        this.coord.y++;
    }

    moveLeft() {
        this.coord.x--;
    }

    moveRight() {
        this.coord.x++;
    }

    getDirectionMethod() {
        return `move${this.direction.charAt(0).toUpperCase() + this.direction.slice(1)}`;
    }

    updateStep() {
        this.step--;

        if (!this.step) {
            this.updateTurn();

            this.step = this.steps;
        }
    }

    updateTurn() {
        this.turn--;

        this.changeDirection()

        if (!this.turn) {
            this.turn = this.constructor.turns;
            this.steps++;
        }

    }

    changeDirection() {
        const directions       = this.getDirections();
        const currentDirection = directions.indexOf(this.direction);
        const nextDirection    = currentDirection + 1;

        this.direction = nextDirection === directions.length ? directions[ 0 ] : directions[ nextDirection ];
    }

    getDirections() {
        const [ up, down, left, right ] = this.constructor.availableDirections;

        if (!this.directions) {
            this.directions = this.clockwise ? [ down, right, up, left ] : [ down, left, up, right ];
        }

        return this.directions;
    }

    getFrom(target) {
        const { x, y } = this.coord;
        const coord = { x, y };
        const value = target[ y ][ x ];

        return {
            coord,
            value
        };
    }

    static get defaultCoord() {
        return {
            y: 0,
            x: 0
        };
    }

    static get defaultDirection() {
        return 'left';
    }

    static get defaultClockwise() {
        return true;
    }

    static get availableDirections() {
        return [ 'up', 'down', 'left', 'right' ];
    }

    static get turns() {
        return 2;
    }

}
